<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Formularz kontaktowy</title>
		
	    <link href="/css/app.css" rel="stylesheet">

    </head>
    <body>
		@yield('content')
		
	    <script src="/js/jquery.min.js"></script>
	    <script src="/js/form.js"></script>
    </body>
</html>
