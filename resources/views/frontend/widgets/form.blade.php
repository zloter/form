<div class="section section--form">


    <form id="form" action="{{ route('ajaxSendForm') }}" method="post" class="form">
        <div class="form__title">
            Formularz kontaktowy
        </div>
        
        {{ csrf_field() }}
        <label for="name" class="form__label form__label--required">
            Imię i nazwisko
        </label>
        <input name="name" type="text" class="form__input">
        
        <label for="email" class="form__label form__label--required">
            Email:
        </label>
        <input name="email" type="text" class="form__input">
        
        <label for="content" class="form__label form__label--required">
            Treść zapytania
        </label>
        <input name="content" type="textarea" class="form__input">

        <input id="allow" name="allow" class="form__checkbox"  type="checkbox">
        <label for="allow" class="form__checkbox-desc">
            * Wyrażam zgodę na przetwarzanie moich danych osobowych przez Zygmunt Loter zgodnie z ustawą z dnia 29 sierpnia 1997r. o ochronie danych osobowych (Dz. U. 2014 r. poz. 1182) w celach marketingowych. Oświadczam, że zostałem poinformowany o przysługującym mi prawie dostępu do treści moich danych osobowych oraz ich poprawiania, jak również prawie wniesienia w każdym czasie sprzeciwu wobec ich przetwarzania.
        </label>

        <input id="sendCopy" name="sendCopy" class="form__checkbox"  type="checkbox">
        <label for="sendCopy" class="form__checkbox-desc">
            Proszę o przesłanie kopii zapytania także do mnie
        </label>

        <div class="form__required">
            * - pola wymagane 
        </div>

        <input type="submit" value="Wyślij zapytanie" class="form__send">
    </form>
    <div class="rotate">
        <div class="rotate__ball"></div>
        <div class="rotate__loading">
            WYSYŁANIE...
        </div>
    </div>
</div>

