
{{-- Greeting --}}


# Witamy!
<br />
<br />

Otrzymałeś nowe zapytanie ze strony internetowej z adresu
<b>
	{{ $email }}
</b>
, od 
<b>
	{{ $name }}
</b>
o następującej treści:
<br />
<br />

{{ $content }}
<br />
<br />
<br />


{{-- Salutation --}}

Pozdrawiamy,<br>CMS Laravel

