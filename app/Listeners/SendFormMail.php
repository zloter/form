<?php

namespace App\Listeners;

use App\Events\FormSendedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;
use App\Mail\FormRequestMail;

class SendFormMail
{

    protected $mailer;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer =  $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  FormSendedEvent  $event
     * @return void
     */
    public function handle(FormSendedEvent $event)
    {
        $data = $event->getData();
        $mail = $this->mailer
            ->bcc('zygmuntloter@gmail.com');

        if (isset($data['sendCopy'])) {
            $mail = $mail->bcc($data['email']);
        }

        $mail->send(new FormRequestMail($data));
    }
}
