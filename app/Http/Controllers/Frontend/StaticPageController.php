<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\View\Factory as View;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{

    protected $view;

    public function __construct(View $view)
    {
        $this->view = $view;    
    }

    public function home()
    {
        return $this->view->make('frontend.home');
    }
}
