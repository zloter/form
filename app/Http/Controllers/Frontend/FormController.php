<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\View\Factory as View;
use App\Http\Controllers\Controller;
use App\Repositories\FormDataRepository;
use App\Http\Requests\FormRequest;
use App\Events\FormSendedEvent;

class FormController extends Controller
{

    protected $view;

    protected $formDataRepository;

    public function __construct(
        View $view,
        FormDataRepository $formDataRepository
    ) {
        $this->view = $view;    
        $this->formDataRepository = $formDataRepository;    

    }

    public function ajaxSendForm(FormRequest $request)
    {
        $data = $request->all();
        $this->formDataRepository->save($data);

        event(new FormSendedEvent($data));
    }
}
