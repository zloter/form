<?php 

namespace App\Repositories\Eloquent;

use App\Repositories\FormDataRepository;
use App\Models\FormData;

class FormDataEloquentRepository implements FormDataRepository
{
    private function model()
    {
        return new FormData();
    }

    public function save($data)
    {
        $this->model()->create($data);
    }
}