<?php 

namespace App\Repositories;

interface FormDataRepository 
{
	/**
	 * save data send through form
	 *
	 * @var $data array
	 *
	 * @return void
	 */
    public function save($data);
}